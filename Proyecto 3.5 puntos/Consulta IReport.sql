-----------------COSNULTA PARA EL IREPORT--------------

select
	sum(fac_total) as Dinero_Anual,
	extract (year from reserva.res_fecha_evento) as Año_Consultado
	
	from 
	factura 
	inner join reserva on reserva.fac_id = factura.fac_id
	inner join area_eventodep on area_eventodep.areae_id = reserva.areae_id
	
	group by extract (year from reserva.res_fecha_evento);
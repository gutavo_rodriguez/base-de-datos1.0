 ---------------------CURSOR--------------------------
 
--DROP FUNCTION expl_cursor1()
-- CASO1 : Uso simple de cursores EXPLÍCITOS 
CREATE OR REPLACE FUNCTION expl_cursor1() RETURNS SETOF void AS
$BODY$
DECLARE
	registro Record;
	
    -- Declaración EXPLICITA del cursor
    cur_clientes CURSOR FOR  
	select extract (year from reserva.res_fecha_evento) as Anio,
	trim(area_eventodep.areae_tipo_descripcion) as tipo_evento,
	count(fac_total) as cantidad_anual,
	sum(fac_total) as recaudacion_anual 
	from 
	factura 
	inner join reserva on reserva.fac_id = factura.fac_id
	inner join area_eventodep on area_eventodep.areae_id = reserva.areae_id
	
	where extract (year from reserva.res_fecha_evento) = 2021
	group by extract (year from reserva.res_fecha_evento),area_eventodep.areae_tipo_descripcion; 
	
    --registro ROWTYPE;
BEGIN
   -- Procesa el cursor
   open cur_clientes;
   fetch cur_clientes into registro;
   while (found) loop
   raise notice 'Anio: %,Evento: %,Cantidad: %, Recaudado:$ %',registro.anio,registro.tipo_evento,
   registro.cantidad_anual,registro.recaudacion_anual;
   fetch cur_clientes into registro;
   END LOOP;
   RETURN;
END $BODY$ LANGUAGE 'plpgsql'

--comprobacion 
select expl_cursor1()

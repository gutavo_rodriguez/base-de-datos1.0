	
	-------------------PROCEDIMIENTO ALMACENADO-------------	
-- •	Como procedimiento almacenado se puede obtener los ingresos obtenidos por una área 
	-- deportiva en especifico ingresando el area a bosquejar
	
	
	CREATE OR REPLACE FUNCTION Valores_AreaEvento(VARCHAR) 
RETURNS real AS  
$BODY$   

 	select --into total
	sum(fac_total)
	from 
	factura 
	inner join reserva on reserva.fac_id = factura.fac_id
	inner join area_eventodep on area_eventodep.areae_id = reserva.areae_id
	where area_eventodep.areae_tipo_descripcion = $1;
$BODY$ 
LANGUAGE SQL;

--comprobacion 
select valores_areaevento('Cancha de Basquet')
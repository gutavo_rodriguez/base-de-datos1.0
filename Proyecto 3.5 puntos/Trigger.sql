----------------------------TRIGGER QUE SI FUNCIONA---------------------
--	Un trigger que haga un aumento salarial del 20% de su salario actual a aquel empleado
--	el cual posea mas de 4 eventos administrados


CREATE OR REPLACE FUNCTION Aumento() RETURNS TRIGGER
AS 
    $Aumento$
DECLARE
    Num_Evento BIGINT ;
	Nombre_empleado varchar;
BEGIN
   	select
	count(reserva.emp_id) into Num_Evento
	from 
	reserva
	inner join empleado on empleado.emp_id = reserva.emp_id
	where reserva.emp_id = new.emp_id
	group by reserva.emp_id,empleado.emp_nombre;
	
	select
	empleado.emp_nombre into Nombre_empleado
	from 
	empleado 
	where empleado.emp_id = new.emp_id;

    IF(Num_Evento < 4) THEN
        RAISE NOTICE 'No aplica para Aumento salarial';
    ELSE
        UPDATE empleado SET emp_sueldo_mensual = emp_sueldo_mensual + (emp_sueldo_mensual * 0.20) 
		WHERE empleado.emp_id = NEW.emp_id;
        RAISE NOTICE 'Aumento realizado a: %', Nombre_empleado;
    END IF;
    RETURN NEW;
END;
$Aumento$
LANGUAGE plpgsql;

CREATE TRIGGER Aumento AFTER
INSERT ON reserva FOR EACH ROW
EXECUTE PROCEDURE Aumento();
--Comprobar que al momento de reserva el trigger se dispare (el valor para aignar el empleado es el 4)
insert into reserva values (default,21,2,7,1,5,'Torneo de futbol sala','2021/12/05','11:00:00','12:00:00',1);--20

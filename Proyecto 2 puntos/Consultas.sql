--CONSULTA 1 
	 
	--Histórico anual de recaudaciones por tipo de evento. En una columna debe 
	--aparecer el año, en otra el tipo de evento, en otra columna la cantidad 
	--de dinero recolectada.
	

	
	select 
	extract (year from reserva.res_fecha_evento) as Anio,
	reserva.res_descripcion as tipo_evento,
	sum(fac_total) as cantidad_anual
	from 
	factura 
	inner join reserva on reserva.fac_id = factura.fac_id
	inner join area_eventodep on area_eventodep.areae_id = reserva.areae_id
	
	where extract (year from reserva.res_fecha_evento) = 2021
	group by extract (year from reserva.res_fecha_evento),reserva.res_descripcion;
	
	--CONSULTA 2
	
--	•	Histórico de número de tipos de campeonatos celebrados en el centro deportivo. 
--	En una columna debe aparecer el año, en otra columna el tipo de evento deportivo, 
--	en otra columna la cantidad de campeonatos para esa disciplina que se han jugado.
	
	select 
	extract (year from reserva.res_fecha_evento) as Anio,
	reserva.res_descripcion as tipo_evento,
	count(fac_total) as num_campeonatos
	from 
	factura 
	inner join reserva on reserva.fac_id = factura.fac_id
	inner join area_eventodep on area_eventodep.areae_id = reserva.areae_id
	
	where extract (year from reserva.res_fecha_evento) = 2021
	group by extract (year from reserva.res_fecha_evento),reserva.res_descripcion;
	
	--CONSULTA 3
	
--	•	Histórico de mantenimientos realizados al año por cada empleado. En una columna
--	 debe aparecer el año, en otra el nombre de empleado, y en otra la cantidad de 
--	mantenimientos que ese empleado ha dado.

	
	
	select 
	extract (year from mantenimiento.man_fecha_mant) as Anio,
	empleado.emp_nombre || ' ' || empleado.emp_apellidos AS Empleado_Asignado,
	count(mantenimiento.man_id) as num_mantenimiento
	from 
	mantenimiento 
	inner join empleado on empleado.emp_id = mantenimiento.emp_id

	
	where extract (year from mantenimiento.man_fecha_mant) = 2021
	group by extract (year from mantenimiento.man_fecha_mant),empleado.emp_nombre,empleado.emp_apellidos;
	
	--CONSULTA 4
	
--	•	Histórico de cantidad de veces que se ha ocupado anualmente cada área del centro. 
--	En una columna debe aparecer el año, en otra columna el nombre del área, en otra 
--	columna la cantidad de veces que esa área fue ocupada ese año.

	select 
	extract (year from reserva.res_fecha_evento) as Anio,
	area_eventodep.areae_tipo_descripcion as tipo_evento,
	count(fac_total) as cantidad_anual
	from 
	factura 
	inner join reserva on reserva.fac_id = factura.fac_id
	inner join area_eventodep on area_eventodep.areae_id = reserva.areae_id
	
	where extract (year from reserva.res_fecha_evento) = 2021
	group by extract (year from reserva.res_fecha_evento),area_eventodep.areae_tipo_descripcion ;
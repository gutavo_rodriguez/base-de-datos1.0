--RODRIGUEZ SAN LUCAS GUSTAVO EMILIO
--CENTRO DE EVENTOS DEPORTIVOS
--4/11/2021
--5TO NIVEL PARALELO A 

CREATE TABLE cliente
(
    client_id SERIAL NOT NULL ,
    client_nombre character(20)  NOT NULL,
    client_apellido character(20) NOT NULL,
    client_ci character(10) NOT NULL,
    CONSTRAINT pk_cliente PRIMARY KEY (client_id)
);



CREATE TABLE servicios_adicionales
(
    paquetad_id SERIAL NOT NULL,
    paquetad_nombre character(30)  NOT NULL,
    paquetad_precio decimal NOT NULL,
    CONSTRAINT pk_servicios_adicionales PRIMARY KEY (paquetad_id)
);

CREATE TABLE tipo_empleado
(
    tipoemp_id SERIAL NOT NULL,
    tipoemp_descripcion character(40)  NOT NULL,
    CONSTRAINT pk_tipo_empleado PRIMARY KEY (tipoemp_id)

);


CREATE TABLE empleado
(
    emp_id SERIAL not null,
    tipoemp_id SERIAL NOT NULL,
    emp_nombre character(25) NOT NULL,
    emp_apellidos character(25)  NOT NULL,
    emp_ci character(10)  NOT NULL,
    emp_direccion character(40)  NOT NULL,
    emp_fechanacimiento date NOT NULL,
    emp_fechainglaborar date NOT NULL,
	emp_sueldo_mensual decimal NOT NULL,
    CONSTRAINT pk_empleado PRIMARY KEY (emp_id),
	
	CONSTRAINT fk1_tipoempleado
      FOREIGN KEY(tipoemp_id) 
	  REFERENCES tipo_empleado(tipoemp_id)
);

CREATE TABLE area_eventodep
(
    areae_id SERIAL NOT NULL ,
    areae_numareadep integer NOT NULL,
    areae_horademantenimiento time without time zone NOT NULL,
    areae_capacidad integer NOT NULL,
    areae_tipo_descripcion character(50) NOT NULL,
	area_costo_alquiler_hora decimal NOT NULL,
	emp_id serial NOT NULL,
	areae_disponibilidad BOOLEAN NOT NULL,
    CONSTRAINT pk_area_eventodep PRIMARY KEY (areae_id),
	
	CONSTRAINT fk1_empleado
      FOREIGN KEY(emp_id) 
	  REFERENCES empleado(emp_id)
	
	
);


CREATE TABLE reserva
(
    res_id SERIAL NOT NULL,
    fac_id SERIAL NOT NULL,
    areae_id SERIAL NOT NULL,
    emp_id SERIAL NOT NULL,
    client_id SERIAL NOT NULL,
    paquetad_id SERIAL NOT NULL,
	res_descripcion character(60) NOT NULL,
    res_fecha_evento date NOT NULL,
    res_hora_inicio time without time zone NOT NULL,
    res_hora_fin time without time zone NOT NULL,
	res_horas_alquiladas int not null,
    CONSTRAINT pk_reserva PRIMARY KEY (res_id),
	
	CONSTRAINT fk1_reserva_area
      FOREIGN KEY(areae_id) 
	  REFERENCES area_eventodep(areae_id),
	
	
	CONSTRAINT fk3_reserva_empleado
      FOREIGN KEY(emp_id) 
	  REFERENCES empleado(emp_id),
	
	CONSTRAINT fk4_reserva_client
      FOREIGN KEY(client_id) 
	  REFERENCES cliente(client_id),
	
	CONSTRAINT fk5_reserva_paquead
      FOREIGN KEY(paquetad_id) 
	  REFERENCES servicios_adicionales(paquetad_id)
	

);

CREATE TABLE factura
(
    fac_id SERIAL  NOT NULL,
	res_id SERIAL NOT NULL,
    fac_fecha DATE NOT NULL,
    fac_subtotal decimal  NOT NULL,
    fac_total decimal NOT NULL,
    fac_iva decimal  NOT NULL,
    CONSTRAINT pk_factura PRIMARY KEY (fac_id),
	
	CONSTRAINT fk1_reserva
      FOREIGN KEY(res_id) 
	  REFERENCES reserva(res_id)
);

CREATE TABLE mantenimiento
(
    man_id serial NOT NULL ,
    emp_id INTEGER NOT NULL ,
    areae_id INTEGER NOT NULL ,
    man_fecha_mant date,
    CONSTRAINT pk_mantenimiento PRIMARY KEY (man_id),
	
	CONSTRAINT fk1_EMPLEADO
      FOREIGN KEY(emp_id) 
	  REFERENCES empleado(emp_id),
	
	CONSTRAINT fk1_Area_Evento
      FOREIGN KEY(areae_id) 
	  REFERENCES area_eventodep(areae_id)
);
